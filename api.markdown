Rabat.Me API 
===
_**version 2.0**_



## Overview
Base URL: 

    http://rabat.me/api/v1/

## Legend

* **[param]** - Optional parameters are given in square brackets
* **\*param** - Not used in the current version, but are under consideration

## 1. Authentication

Simple authentication_token based authentication is used.

JSON is a default format for reply.

### 1.1 Calls

---
#### 1.1.1 Login

    POST /login

---
#### 1.1.1 Logout

    GET /logout


---
## 2. Public API calls

### 2.1. Shops nearby

  The old call with GET params is also working for backward compatibility. However it must be changed ASAP.

    GET /shops/near/:lat/:lng(/:radius)(/:unit)
    
**Params**

  > lat
  
  > lng
  
  > [radius] 
  
  > [unit] - default km
  
### 2.2. Customer shop accounts

    GET /customer/:code/shops
    
**Params**
    
  > code - Customer QR Code



---
## 3. Protected API calls

All the calls below must supply authentication_token parameter obtained during the authentication process.

You can add auth_token as value for "X-Rabatme-Auth-Token" HTTP Header field

### 3.1. Products

    GET /products

**Output**

  Array of products
  
    id          - String 
    name        - String
    points      - Integer
    description - String
    icon        - String

### 3.2. Rewards

    GET /rewards
    
**Output**

  Array of rewards
  
    id          - String 
    name        - String
    points      - Integer
    description - String
    icon        - String  

### 3.3. Customer account for the current shop

    GET /customer/:code/shop

**Params**
    
    > code - Customer QR Code
    
**Output**

### 3.4. Stamp - scan a customer code

    POST /stamps
    
**Params**

  > code - Customer QR Code
  
  > products - Array of pairs {product_id, amount}
  
    Example in JSON
    {
      "code"=>"rabatmewbVXGcs", 
      "products"=>[
        {"product_id"=>"4fa7fca957e64a0c9a00000f", "amount"=>"4"}, 
        {"product_id"=>"4fa7fbde57e64a0c9a000009", "amount"=>"4"}
      ]
    }
  

**Output**
    
    
    
### 3.4. Claim rewards

    POST /claim
    

**Params**

  > code - Customer QR Code

  > rewards - Array of pairs {product_id, amount}

    Example in JSON
    {
      "code"=>"rabatmewbVXGcs", 
      "rewards"=>[
        {"reward_id"=>"4fa7fca957e64a0c9a00000f", "amount"=>"4"}, 
        {"reward_id"=>"4fa7fbde57e64a0c9a000009", "amount"=>"4"}
      ]
    }


**Output**

    
    
    
  










    
