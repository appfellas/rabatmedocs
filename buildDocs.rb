# gem install redcarpet
# gem install coderay
require "redcarpet"
require 'coderay'

#TODO add language support for highlight
class HTMLwithAlbino < Redcarpet::Render::HTML
  #def block_code(code, language)
    #CodeRay.scan(code, :html).div(:line_numbers => false)
    #code
  #end
end

file = "./html/api.html"
source = "./api.markdown"
headerTpl = "./tpl/header.html"
footerTpl = "./tpl/footer.html"

renderer = HTMLwithAlbino.new({
  :with_toc_data => true,
  :fenced_code_blocks => true
})

markdown = Redcarpet::Markdown.new(renderer, 
  :autolink => true, 
  :space_after_headers => true
)

targetFile = File.open(file, 'w')

html = markdown.render(File.read(source))

header = File.read(headerTpl)
footer = File.read(footerTpl)

outHtml = header + html + footer

targetFile.write(outHtml)
targetFile.close()

puts "Docs are built"
#html = GitHub::Markup.render(file, File.read(source))



