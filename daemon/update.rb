#
# Update from git and 
#
# Run 
# screen -L -dmS rmupdater ruby update.rb -p 8081 
# to start this daemon on port 8081
# 
# To shutdown the script run
# screen -r  rmupdater 
# and then press ctrl+c

require 'rubygems'
require 'sinatra'

get '/' do
  update_docs
end

post '/' do
  update_docs
end

def update_docs
  output = IO.popen('git pull origin master 2>&1')
  puts output.readlines
  output = IO.popen('cd .. && ruby buildDocs.rb')
  puts output.readlines
  
end  